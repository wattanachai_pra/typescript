import { TypeScriptPage } from './app.po';

describe('type-script App', function() {
  let page: TypeScriptPage;

  beforeEach(() => {
    page = new TypeScriptPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
