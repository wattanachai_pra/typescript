/* tslint:disable:no-unused-variable */

import { addProviders } from '@angular/core/testing';
import {Category} from './category';
import {Product} from './product';
import {beforeEach, beforeEachProviders, describe, xdescribe, expect, it, xit, async, inject} from '@angular/core/testing';

describe('Test Category Model', () => {
    it('should construct a Category Correctly', () => {
        const productObj = {
          id:1,
          name:'product name'
        }
        const categoryObj = {
            id: 1,
            name: 'Category Name',
            product: productObj
        }
        const category = new Category(categoryObj);
        expect(category.id).toBe(1);
        expect(category.name).toBe('Category Name');
    });
});

