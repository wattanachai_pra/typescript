import {Product} from '../models/product';
export class Category {
    constructor(
        category?: any) {
        this.id = category.id;
        this.name = category.name;
        this.product = category.product;
    }
    id: number;
    name: string;
    product: Product;
}
