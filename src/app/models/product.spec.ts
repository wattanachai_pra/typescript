/* tslint:disable:no-unused-variable */

import { addProviders } from '@angular/core/testing';
import {Product} from './product';
import {beforeEach, beforeEachProviders, describe, xdescribe, expect, it, xit, async, inject} from '@angular/core/testing';

describe('Test Product Model', () => {
  it('should construct a Product Correctly', () => {
    const productObj = {
      id: 1,
      name: 'product name'
    }

    const product = new Product(productObj);
    expect(product.id).toBe(1);
    expect(product.name).toBe('product name');
  });
});

