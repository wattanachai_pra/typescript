export class Product {
    constructor(
        product: any) {
        this.id = product.id;
        this.name = product.name;
    }
    id: number;
    name: string;
}
