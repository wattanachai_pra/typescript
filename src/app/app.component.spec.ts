/* tslint:disable:no-unused-variable */

import { addProviders } from '@angular/core/testing';
import { AppComponent } from './app.component';

import {beforeEach, beforeEachProviders, describe, xdescribe, expect, it, xit, async, inject} from '@angular/core/testing';
import { provide  } from '@angular/core';
import { MockBackend  } from '@angular/http/testing';
import { Http, HTTP_PROVIDERS, Response, ResponseOptions, BaseRequestOptions, ConnectionBackend, XHRBackend} from '@angular/http';
import {Category} from '../app/models/category';
import {Product} from '../app/models/product';

describe('App: TypeScript', () => {
  beforeEach(() => {
    addProviders([AppComponent]);
  });

  it('should create the app',
    inject([AppComponent], (app: AppComponent) => {
      expect(app).toBeTruthy();
    }));

  it('should have as title \'app works!\'',
    inject([AppComponent], (app: AppComponent) => {
      expect(app.title).toEqual('app works!');
    }));
});
