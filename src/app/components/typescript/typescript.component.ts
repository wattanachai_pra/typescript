import { Component, OnInit } from '@angular/core';
import {Product} from '../../models/product';
import {Category} from '../../models/category';


@Component({
  moduleId: module.id,
  selector: 'app-typescript',
  templateUrl: 'typescript.component.html',
  styleUrls: ['typescript.component.css']
})
export class TypescriptComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
