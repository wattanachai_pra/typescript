/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders } from '@angular/core/testing';
import { TypescriptComponent } from './typescript.component';
import {beforeEach, beforeEachProviders, describe, xdescribe, expect, it, xit, async, inject} from '@angular/core/testing';

describe('Component: Typescript', () => {
  it('should create an instance', () => {
    let component = new TypescriptComponent();
    expect(component).toBeTruthy();
  });

  it('Class', () => {

    class Point {
      x: number;
      y: number;
      constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
      }
      add(point: Point) {
        return new Point(this.x + point.x, this.y + point.y);
      }
    }

    var p1 = new Point(0, 10);
    var p2 = new Point(10, 20);
    var p3 = p1.add(p2);
  })

  it('Arrow Functions', () => {
    class Product {
      id: number;
      name: string;
      constructor(product?: any) {
        this.id = product.id;
        this.name = product.name;
      }
    }

    class Variant {
      id: number;
      name: string;
      constructor(variant?: any) {
        this.id = variant.id;
        this.name = variant.name;
      }
    }

    let product1 = new Product({ id: 1, name: 'product1' });
    let product2 = new Product({ id: 2, name: 'product2' });
    let product3 = new Product({ id: 3, name: 'product3' });
    const productList = [product1, product2, product3];
    // productList.forEach(product => console.log(product))
    let productIdList = productList.filter(product => product.id < 3)
    let productNameList = productList.filter(product => product.name === 'product1');
    let variantList = productList.map(product => {
      let variant = new Variant(product);
      let numericFromName = variant.name.match(/\d+/g).map(Number);
      variant.name = `variant${numericFromName}`
      return variant;
    })

    function add(a: number, b: number) {
      return a + b;
    }
    let addArrow = (a, b) => a + b
    let sum = productList.map(product => product.id).reduce((id, sumId: number) => sumId = add(sumId, id))
    let sumReduce = productList.map(product => product.id).reduce((id, sumId: number) => sumId = addArrow(sumId, id))
  })

  it('Enum', () => {
    enum Color {
      Red,     // 0
      Green,   // 1
      Blue     // 2
    }
  })

  it('undefine and Null', () => {
    let undefined1 = undefined;
    let undefined2 = undefined;
    let null1 = null;
    let null2 = null;
    // console.log(undefined == undefined); // true
    // console.log(null == undefined); // true”

    expect(undefined1).toBe(undefined1);
    expect(null1).toBe(null2)
  })

  it('Tuper', () => {
    var nameNumber: [string, number];
    nameNumber = ['Jenny', 8675309];
    // nameNumber = ['Jenny', '867-5309']; '[string, string]' is not assignable to type '[string, number]'.

  })
});
